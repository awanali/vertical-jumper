﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Platform : MonoBehaviour
{
    [SerializeField]
    private float _jumpForce = 10f;


    private void Start()
    {
    }

    private void Update()
    {
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Rigidbody2D rigidbody2D = collision.gameObject.GetComponent<Rigidbody2D>();
        if (rigidbody2D != null)
        {
            rigidbody2D.velocity = new Vector2(rigidbody2D.velocity.x, _jumpForce);
        }
    }

    private void OnBecameInvisible()
    {
        transform.position = new Vector2(Random.Range(-2f, 2f), Random.Range(transform.position.y + 10f, transform.position.y + 11f));
        /* Destroy(gameObject); */
    }
}
