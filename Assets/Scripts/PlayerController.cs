﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;


public class PlayerController : MonoBehaviour
{

    [SerializeField]
    private float _moveSpeed = 50f;

    [SerializeField]
    private AudioClip _hitSound, _jumpSound;

    [SerializeField]
    private Text _scoreText;

    private AudioSource _audioSource;

    private Rigidbody2D _rigidBody2D;

    private float _moveX;

    private int _score = 0;

    // Start is called before the first frame update
    void Start()
    {
        _rigidBody2D = GetComponent<Rigidbody2D>();
        _audioSource = transform.GetComponent<AudioSource>();
    }

    // Update is called once per frame
    void Update()
    {
        _moveX = Input.acceleration.x * _moveSpeed;
    }

    void FixedUpdate()
    {
        _rigidBody2D.velocity = new Vector2(_moveX, _rigidBody2D.velocity.y);
    }

    void OnCollisionExit2D()
    {
        // Play hit sound
        _audioSource.PlayOneShot(_jumpSound);
        // Increase score
        _score++;
        // Update score text
        _scoreText.text = _score.ToString();
    }

    void OnTriggerEnter2D(Collider2D col)
    {
        // Play hit sound
        _audioSource.PlayOneShot(_hitSound);
        // Reload scene to reset game
        SceneManager.LoadScene(0);
    }
}
